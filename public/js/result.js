(function($){
   var highlightTimes = [];
   // chart configs 

    // getData with query
    //

    var getMarkerCall = function(tid) {
        var qUrl = '/getMarker?tid='+tid;
        return $.ajax({
            url : qUrl,
            method : "GET"
        });
    }

    var getClusterCall = function() {
        var qUrl = '/getCluster'
        return $.ajax({
            url : qUrl,
            method : "GET"
        });
    }

    var testClusterCall = function(param) {
        return $.ajax({
            url : "/testCluster",
            data : param,
            method : "POST"
        });
    }

    // helper functions
    //
    var term = 5;
    var videoLength = 753;
    function sDataSet () { 
        this.label = "scatter data"
    }    
    var normWtTime = function(rawdata) {
        var length = Math.floor(videoLength / term) + 1;
        var normResult = Array(length);
        normResult.fill(0);

        rawdata.forEach(function(rd) {
             var idx = Math.floor(rd.createdAt / term);
             normResult[idx] += 1; 
        });
        return normResult;

    }
    
    var clusterData;
    var lineDataSet = {
        label: "Marker Data",
        fill: false,
        backgroundColor : 'rgba(75, 192, 192, 0.4)',
        borderColor: "rgba(75, 192, 192, 1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle : 'miter',
        pointBorderColor: "rgba(75, 192, 192, 1)",
        pointBackgroundColor : "#fff",
        pointBorderWidth : 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75, 192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        spanGaps: false,
    }

    var countPeopleCall = function(tid) {
        var url = tid ? "/countpeople?tid=" + tid : "/countpeople";
        return $.ajax({
            url : url,
            method : "GET"
        });
    }
    var markerArray;
    var clusters;
    var makeLabels = function(interval, videoLength) {
        var labels = Array();
        var temp = 0;
        while (temp < videoLength) {
            labels.push(temp);
            temp += interval;
        }
        return labels;
    } 
    
    var addClickClusterHandler  = function() {
        $("#clusterBar").on('click', '.cluster', function() {
            showHighlight($(this).index() - 1);
        });
    }
    var player = videojs("demo");
    player.ready(function(){
        player.pause();
    
    });
    var curPlaybackTimeout;
    var showHighlight = function(idx) {
        var moment = highlightTimes[idx];
        player.currentTime(moment[0]);
        player.play();
        
        clearTimeout(curPlaybackTimeout);
        curPlaybackTimeout = setTimeout(function() {
            player.pause();
        }, (moment[1] - moment[0]) * 1000 + 1);


    }
    var showHighlightTimes = function() {
        $(".highlightTimes").html("");
        highlightTimes.forEach(function(interval){
            var p = $("<p></p>");
            p.append(interval[0] + " " + interval[1]);
            $(".highlightTimes").append(p);
        });
    }
    var goldStart = [129,185,214,241,258,291,306,424,500,528,566,583,597,663,675];
    var goldEnd =   [135,198,221,245,266,293,311,429,503,532,572,592,605,666,690];
    
    var showGoldHighlight = function(idx){
        var start = goldStart[idx];
        var end = goldEnd[idx];
        player.currentTime(start);
        player.play();
        clearTimeout(curPlaybackTimeout);

        curPlaybackTimeout = setTimeout(function() {
            player.pause();
        }, (end - start) * 1000 + 1);
    }
    var addClickGoldHandler = function(){
        $("#clusterBar").on('click', '.clusterGold', function() {
            showGoldHighlight($(this).index());
        });
    }
    var showResult = function() {
        highlightTimes = [];
        var tid = $("input[name=time]").val();

        getMarkerCall(tid).done(function(data) {
           lineDataSet.data = normWtTime(data);
            renderChart([lineDataSet]);
        });

        var reqRad = $("input[name=radius]").val();
        var reqNei = $("input[name=neighbor]").val();

        var param = {
            radius : reqRad,
            neighbors : reqNei,
            tid : tid
        };
        testClusterCall(param).done(function(res) {
            clusterData = res[0];
            var clusterBar = $("#clusterBar");
            clusterBar.html("<div id='goldBar'></div>");
            clusterData.forEach(function(cItem){
                highlightTimes.push(cItem);
                var start = cItem[0];
                var end = cItem[1];

                var chunks = $("<div class=cluster></div>");
                chunks.css("left", (start / videoLength) * 100 + "%");
                chunks.css("width", ((end-start) / videoLength) * 100+"%" );
                clusterBar.append(chunks);
                addClickClusterHandler();
            });

            //showHighlightTimes();
            var goldBar = $("#goldBar");
            for (i = 0; i < goldStart.length; i++) { 
                var start = goldStart[i];
                var end = goldEnd[i];
                var chunks = $("<div class=clusterGold></div>");
                chunks.css("left", (start / videoLength) * 100 + "%");
                chunks.css("width", ((end-start) / videoLength) * 100+"%" );
                goldBar.append(chunks);
                addClickGoldHandler();
            }
        });

        countPeopleCall(tid).done(function(res) {
            var num = $(".num");
            num.html("");
            var numOfPeople = $("<p></p>");
            numOfPeople.append(res.length + " people participated ");
            num.append(numOfPeople);
        });
    }
    
    var chObj = {};
    chObj.datasets = Array();
    chObj.labels = makeLabels(term, videoLength);

    var clearChart = function(data) {
        $("#canvas-wrapper").html("").html("<canvas id='resultChart'>");
    }
    var renderChart = function(data) {
        data.forEach(function(item) {
            chObj.datasets.push(item);
        });
        $("#canvas-wrapper").html("").html("<canvas id='resultChart'>");
        var ctx = $('#resultChart');
        var chartInstance = new Chart(ctx, {
            type : 'line',
            data : chObj
        });
    }
    // main function
    //
    
    $(document).ready(function() {
        $("#getResult").click(showResult);
        $("#clearChart").click(clearChart);
    });



})(jQuery);



