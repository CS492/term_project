(function($) {

    var socket = io.connect('');

    var myMarkers = [];
    var otherMarkers = [];

    var highlightTimes = [];
    
    var curPlaybackTimeOut;
    var isPlayerReady = false;
    
    var preventTime = 5000;

    var loadThumbnails = function(data) {
        // First clean original ones

        var date = new Date();
        var curHour = date.getHours();
        var order = highlightTimes.length;
        
        console.log("loadThumbnails: "+ order);

        getClusterCall(order).done(function(data) {
            if (data) {
                // draw thumbnails
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    var thumbnailDiv = $("<div class='thumbnailDiv'></div>");
                    var thumbnailCap = $("<p></p>");
                    thumbnailDiv.data("index", highlightTimes.length - 1);
                    var begin = Math.floor(item.beginAt);
                    var beginMin = Math.floor(begin / 60);
                    if(beginMin < 10)
                        beginMin = '0' + beginMin;
                    var beginSec = Math.floor(begin % 60);
                    if(beginSec < 10)
                        beginSec = '0' + beginSec;
                    var end = Math.floor(item.endAt);
                    var endMin = Math.floor(end / 60);
                    if(endMin < 10)
                        endMin = '0' + endMin;
                    var endSec = Math.floor(end % 60);
                    if(endSec < 10)
                        endSec = '0' + endSec;
                    thumbnailCap.html("Highlight " + (item.highlightID) + " (" + beginMin + ":" + beginSec + " ~ " + endMin + ":" + endSec + ")");
                    
                    var img = document.createElement('img');
                    img.src = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Highlight!&w=300&h=150';

                    generateAsyncThumbnail(img, (begin+end)/2, i)

                    thumbnailDiv.append(thumbnailCap);
                    thumbnailDiv.append(img);
                    $(".generate-highlights").prepend(thumbnailDiv);
                }
            }
        });
    };

    var generateAsyncThumbnail = function(img, time, i) {        
        setTimeout(function () {
            console.log(i);
            var thumbnail = document.createElement('canvas');
            thumbnail.setAttribute('crossOrigin','anonymous');
            thumbnailPlayer.currentTime(time);     
            setTimeout(function () {
                var video = document.getElementById('thumbnailPlayer_html5_api');
                thumbnail.getContext('2d').drawImage(video, 0, 0, 300, 150);
                img.src = thumbnail.toDataURL();
            }, 4000);
        }, i * 5000);
    }

     var getMarkerCall = function() {
        var date = new Date();
        var hour = date.getHours();
        return $.ajax({
            url : '/getMarker?tid=' + hour,
            method : 'GET',
            async: false
        });
    }

    var getClusterCall = function(order) {
        var date = new Date();
        var hour = date.getHours();
        console.log(order);
        var qUrl = '/getCluster?t=' + hour + '&order=' + order;
        return $.ajax({
            url : qUrl,
            method : 'GET'
        });
    
    }

    var testClusterCall = function(param){
        return $.ajax({
            url : "/testCluster",
            method : "POST",
            data : param
        });
    }

    var addThumbnailClickListener = function() {
        $('.generate-highlights').on('click', '.thumbnailDiv', function(){
            showHighlight(highlightTimes.length - $(this).index() - 1);
            $(".thumbnailDiv").removeClass("currentHighlight");
            $(this).addClass("currentHighlight");
        })
    }

    /*
    var clipThumbnail = function(time, clusterId) {
        var thumbnail = document.createElement('canvas'); 
        thumbnailPlayer.currentTime(time);
        var video = document.getElementById('thumbnailPlayer_html5_api');
        var order = $('.thumbnailDiv').length;

        // append thumbnail
        if (clusterId){
            setTimeout(function(){
                thumbnail.getContext('2d').drawImage(video, 0, 0, 300, 150);
                var base64 = thumbnail.toDataURL();
                //
                // To save thumbnail
                //
                $.ajax({
                    method: 'POST',
                    url: 'toThumbnail',
                    data : {
                        img : base64,
                        clusterId: clusterId
                    } 
                }).done(function(data) {
                    loadThumbnails(data);
                    setTimeout(function() {
                        addThumbnailClickListener();
                    }, 100)
                });    
            }, 5000);
        }
          
    };
    */

    /*
    var captureThumbnail = function(time) {
        var thumbnail = document.createElement('canvas');
        thumbnailPlayer.currentTime(time);
        var video = document.getElementById('thumbnailPlayer_html5_api');
        thumbnail.getContext('2d').drawImage(video, 0, 0, 300, 150);
        var thumbnailDiv = $("<div class='thumbnailDiv'></div>");
        var thumbnailCap = $("<p></p>");
        var begin = Math.floor(item.beginAt);
        var beginMin = Math.floor(begin / 60);
        if(beginMin < 10)
            beginMin = '0' + beginMin;
        var beginSec = Math.floor(begin % 60);
        if(beginSec < 10)
            beginSec = '0' + beginSec;
        var end = Math.floor(item.endAt);
        var endMin = Math.floor(end / 60);
        if(endMin < 10)
            endMin = '0' + endMin;
        var endSec = Math.floor(end % 60);
        if(endSec < 10)
            endSec = '0' + endSec;
        thumbnailCap.html("Highlight " + (item.highlightID) + " (" + beginMin + ":" + beginSec + " ~ " + endMin + ":" + endSec + ")");
        thumbnailDiv.append(thumbnailCap);
        thumbnailDiv.append(thumbnail);
        $(".generate-highlights").prepend(thumbnailDiv);
    }
    */

    var getLastMoment = function() {
        var date = new Date();
        var tid = date.getHours();
        var param = {
            radius : 10,
            neightbor : 3,
            tid : tid
        }
        testClusterCall(param).done(function(data){
            var clusterData = res[0];
            var lastCluster = clusterData[clusterData.length - 1];
            highlightTimes.push(lastCluster);
            // captureThumbnail((lastCluster[0] + lastCluster[1]) / 2 );
            addThumbnailClickListener();
        });
    }

    var highlightPlayer = videojs('highlightPlayer');
    highlightPlayer.ready(function(){
        highlightPlayer.pause();
        highlightPlayer.disableProgress({
            autoDisable: true
        });
    });

    var showHighlight = function(id) {
        $('#highlightModal').modal('show');
        player.muted(true);

        // set player time
        highlightPlayer.currentTime(highlightTimes[id][0]);
        highlightPlayer.play();

        // stop after given time
        clearTimeout(curPlaybackTimeOut);
        curPlaybackTimeOut = setTimeout(function() {
            highlightPlayer.pause();
        }, ((highlightTimes[id][1]-highlightTimes[id][0])*1000)+1);

    };

    var hideHighlight = function() {
        player.muted(false);
        highlightPlayer.pause();
        clearTimeout(curPlaybackTimeOut);
    }

    var curMarkerIndex;
    var curAjaxCall;

    var visualMarker = function(curPlayedTime, curUser) {
        addMyMarker({
            time: curPlayedTime, 
            text: curUser
        });

        curMarkerIndex = player.markers.getMarkers().length - 1; 
        console.log(curMarkerIndex);

        // show toast
        var x = document.getElementById("snackbar")
        x.className = "show";
        setTimeout(function(){ 
            x.className = x.className.replace("show", ""); 
        }, 2000);
    };

    var deadLock = false;

    var preventMash = function() {
        deadLock = true;
        var undo = $('#undo');
        var submitButton = $('#highlight');

        undo.removeClass('disabled');
        submitButton.addClass('disabled');

        // after preventTime passed, back on original environment
        setTimeout(function() {
            undo.addClass('disabled');
            submitButton.removeClass('disabled');
            submitButton.removeData("toggle");
            deadLock = false;
        }, preventTime);
    }

    var clickHighlight = function() {
        if(isPlayerReady && sessionTime != -1)
        {
            var curPlayedTime = player.currentTime()
            var curUser = $('#user').html();

            // ajax call to server saving marker
            if (!deadLock) {
                var date = new Date();
                var curHour = date.getHours();
                preventMash();
                visualMarker(curPlayedTime, curUser);
                curAjaxCall = setTimeout(function() {
                    $.ajax({
                        method : 'POST',
                        url    : '/setMarker',
                        data : {
                            createdAt : curPlayedTime,
                            createdBy : curUser,
                            tid : curHour
                        }
                    }).done(function(data) {
                        console.log("setMarker");
                        if(data)
                        {
                            // emit event to socket server
                            socket.emit('fromclient', {
                                createdAt: curPlayedTime, 
                                createdBy: curUser
                            });
                        }
                        
                    });
                }, preventTime);
            }
        }
    };

    var clickUndo = function() {
        // style and disabling css
        var undo = $('#undo');
        var submitButton = $('#highlight');
        undo.addClass('disabled');
        submitButton.removeClass('disabled');
        

        if (deadLock) {
                        // api for undoing marker stuff
            // undoMarker(req, function());
            var allmarkers = player.markers;
            console.log(curMarkerIndex);
            allmarkers.remove([curMarkerIndex]);

            // clearing timeout

            clearTimeout(curAjaxCall);

            console.log("cleared");
        }
        deadLock = false;
        
    }

    var initMarker = function(data) {
        var markerData = [];

        data.forEach(function(element) {
            markerData.push({
                time: element.createdAt,
                text: element.createdBy
            });
        });

        console.log(markerData);

        player.markers({
            markerStyle: { 
                'width': '7px',
                'border-radius': '30%',
                'background-color': 'red'
            },
            markerTip:{
                display: true,
                time: function(marker) {
                    return marker.time;
                },
                text: function(marker) {
                    return "Created By: " + marker.text;
                }
            },
            markers: markerData
        });
    }

    var addMyMarker = function(data) {
        if(isPlayerReady)
        {
            player.markers.add([{
                time: data.time,
                text: data.text,
                class: "special-blue"
            }]);
        }
    }

    var addNormalMarker = function(data) {
        if(isPlayerReady)
        {
            player.markers.add([{
                time: data.time,
                text: data.text
            }])
        }
    }

    var addHighlight = function(data) {
        console.log(data);
        highlightTimes.push(data);
    }

    var player = videojs('demo');

    player.disableProgress({
        autoDisable: true
    });

    player.ready(function(){

        getMarkerCall().done(function(data){
            console.log("Marker will be inited.");
            initMarker(data);
        });

        if(sessionTime != -1 && sessionTime < videoLength)
        {
            player.play();
        }

        if(sessionTime != -1 && sessionTime < videoLength)
        {
            player.currentTime(sessionTime);
            
            // For Safari...
            setTimeout(function() {
                player.currentTime(sessionTime + 3);
            }, 3000);
        }

        player.on('ended', function() {
            location.reload();
            getLastMoment();
        });

        console.log("player is ready.");

        isPlayerReady = true;
    });

    var thumbnailPlayer = videojs('thumbnailPlayer');
    thumbnailPlayer.pause();
    thumbnailPlayer.ready(function() {
        
        console.log("thumbnail player is ready.");

        getClusterCall(0).done(function(data) {
            data.forEach(function(item) {
                console.log(item);
                addHighlight([item.beginAt, item.endAt]);
            });
        });
    });

    socket.on('toclient', function(data) {
        if(isPlayerReady)
        {
            addNormalMarker({
                time: data.createdAt,
                text: data.createdBy
            });
        }
    });

    socket.on('newCluster', function(data) {
        if(isPlayerReady)
        {
            var interval = [data[0]-5, data[1]+5];
            var clusterId = data[2];
            addHighlight(interval);
            //clipThumbnail((data[0] + data[1]) / 2, clusterId);
            loadThumbnails();
            console.log("new Cluster is made!");
        }
    });

    var loadTooltip = function() {
        $('[data-toggle="tooltip"]').tooltip(); 
    }

    $(document).ready(function() {

        console.log("document is ready");

        $('#highlight').click(clickHighlight);
        $('#undo').click(clickUndo);

        $('#highlightModal').on('hidden.bs.modal', hideHighlight);

        setTimeout(function() {
            addThumbnailClickListener();
        }, 1000);

        loadThumbnails();
        loadTooltip();
    
    });

})(jQuery);

