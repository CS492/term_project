## What is the problem your team is trying to solve?

Adding more metadata to the video, to make it searchable.

## How do we know this problem exists? Why is it important?
### Include (1) external references, (2) internal investigation
(1) [Video Indexing Based on Mosaic Representation](http://www.wisdom.weizmann.ac.il/~irani/PAPERS/video-indexing.pdf)
There were several approaches for video indexing.

(2) For personal experience, I felt difficulties while searching video. 
Especially, for long video ( more than 30min ), the part that I really want is buried for many times.

## Why use crowdsourcing for the problem? Why not use machines or a small group of experts?

Up to my knowledge, there are still little improvement in image processing with machine. 
Also, to deal with bunch of video in the web, small group of experts is not proper.
Crowdsourcing can deal with it.

## What specific challenges exist?

HMW give motivation to the watcher?

HMW select target videos?

HMW implement the system?

HMW gather user? ( who will be our user? )

HMW aggregate the collected data?

HMW control the noise?

## What is solution?

One kind of solution is that make plugin, addon that save bookmark for the youtube-video.
Then it archived to the system. After that system is used for other user.

* For the point of requester.
    This system is powered by instrinsic behavior by watchers. 
* For the point of workers
    At the point of workers. They mark the bookmark while they view the video. 
    We would notice them, the marking behavior will give a benefit to the another watchers.
    They mark not only the specific period, interval of the video, but also they will 
    give the tag, description of the video. This tag, description will be helpful
    when searching video.

### Analyze the idea using the seven dimensions above. 

* Motivation - why would a crowd worker do this?
    * Crowd worker's natural behavior ( marking bookmark, tags... ) that want to 
    rewatch video. This is motivation
* Aggregation - how are results from multiple workers combined?
    * For the same video, the bookmarks from multiple workers will be piled up.
    * Crowd can view the tags, and they can filter out what is good tags or bad tags.
* Crowd pool - you are required to use a VOLUNTARY OR INTRINSICALLY MOTIVATED CROWD, but please be more specific.
    * Crowds are VOLUNTARY AND INTRINSICALLY MOTIVATED
    * All the users who wants video is in our crowd pool
* Quality control - how to ensure valid results?
    * They can review previous workers' bookmarks and report to the system.
* Human skill - what kind of human skill is required to complete a task?
    * Basic bookmarking skill is needed to complete a task.
* Process order - in what order is the work processed between computer, worker, and requester?
    * In this solution, there is no specific requester, all the videos in the web could be a target.
    * System only run in the background and help when workers want to tag them.
    * Workers just do their bookmarking.
*   So the process order would be 
    *   1. videos on the website
    *   2. workers watching videos
    *   3. System deals with collected data
* Goal visibility - how much of the overall goal of the system is visible to an individual crowd worker?
    * All workers can know what they are doing since we will announce them their work will be a help for our system.



