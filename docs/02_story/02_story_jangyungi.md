# Tasks, Sketches, and Video

Make highlight videos from live streams by the crowd.

## Tasks

### Task 1

Mr. Shimada, 38, an entrepreneur and market surveyor who wants to find some information from a huge bunch of videos possibly related.

* The tagging/annotating could be made by non-video owners because pre-defined annotations when the videos were uploaded are not available.

* Visual cues like Thumbnails for each moment should be provided in order to help the surveyor to search multiple videos.

* The response should be made quickly, because if it is slower than skimming all the videos to find the information that he might want to find, it would be nonsense!

### Task 2

Ms. Song, 19,  a popular Youtube streamer, who wants her videos even more popular by making them more discoverable and searchable.
          
* The annotation/tagging process should be simple and non-time-consuming, in case the uploader wants to annotate it by herself.

* If we want the highlight moments of the video could be annotated, possibly this can be done by the audiences. Annotations from steamed videos could be easily used again for edited and recorded version of videos.

* As she has a lot of followers, the fans are likely to give a huge amount of annotations. Aggregation of those massive amounts of data should be prepared.

### Task 3

Ms. Ziegler, 37, a casual Youtube contents consumer who wants to track videos that impressed her with bookmarks.

* Control over share/not to share. The annotation should be shared somehow to build the public database. On the other hand, for privacy, some video annotations should not be shared.

* Due to limitations of memory, visual and even sound cues should be provided.

* The bookmarks should be permanence guaranteed. Because this is for personal review purpose. For this, log-in or other means to ensure one’s bookmarks are not lost.