# STORY_BOARD

### Task1
Jiwoo, 24-year old huge fan of sports (NBA, KBL, e-Sports), loves watching sports
live. After a long term video, he wants to review the highlight film of the game.
But producing highlight film costs. It takes from 1hr to 1day and even there is 
situation that no highlight film at all.

#### Design Goal for the task.

* Video should be generated quick after the end of the game.
* Result video should contain all of highlights in the game and the total length not shorter than 10 min.
* Video should describe the context of the situation. It should not cut the atmosphere of the situation.

### Task2
Jiwoo, 24-year old college student, who love watching entertainment shows in TV.
Like, infinite challenge... But he has not enough time to enjoy it. So he wants to
watch clipped video as a substitute. Challenge for that it is hard to get it.

#### Design Goal for the task

* Video should contains whole funny parts in the show.
* Also, Jiwoo could understand whole context of the video through the clipped video.
* 10 min ~ 15 min length is appropriate

