// server.js
//
// set up =================================================================
// get all the tools we need
var express = require('express');
var path = require('path');
var app = express();
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var http = require('http');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');
var dburl = configDB.cloudurl;
if (process.env.NODE_ENV == "d") {  // d is for development
    dburl = configDB.url;
}
mongoose.connect(dburl, function() {
    console.log("connected to " + dburl);    
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// required for passport
require('./config/passport')(passport); // pass passport for configuration
app.use(session({ secret : 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// socket.io
var httpServer = http.createServer(app).listen(port, function(req, res) {
    console.log('Socket IO server has been started')    ;
});

var io = require('socket.io').listen(httpServer);

io.sockets.on('connection', function(socket) {
    socket.on('fromclient', function(data) {
        if (data.createdBy !== undefined) {
             var response = {
                createdAt : data.createdAt,
                createdBy : data.createdBy
            }
            socket.broadcast.emit('toclient', response);
       
        }
    })
});
require('./app/routes.js')(app, passport, io);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// for socket io
//

