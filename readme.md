# LiveLight: crowd generated highights

## Project Summary

It's a nice project

## Instruction

Give a quick tour of the interface, and also show off some of the highlights of the interface.


## Technical Description

What frameworks / libraries / technical stack have been used for both the frontend and the backend.

## Prototype URL

You can use our prototype here: https://live-light.herokuapp.com/

## Prototype Code

The repository of Livelight is available at https://gitlab.com/CS492/term_project.git
