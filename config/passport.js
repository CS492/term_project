// config/passport.js
//

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;

// load up the user model
//
var User = require('../app/models/user');


// expose this function to our app using module.exports
module.exports = function(passport) {
    // passport session setup
    //
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    passport.serializeUser(function(user, done) {
       done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
       User.findById(id, function(err, user) {
          done(err, user);
       });
    });

    // local signup
    //
    // using named strategies since we have one for login and one for signup
    // by default, if there was no name it would just be called 'local'
    passport.use('local-signup', new LocalStrategy({
        // default local strategy uses username and password
        usernameField : "username",
        passwordField : 'password',
        passReqToCallback : true
    }, function(req, username, password, done) {
        // asynchronous
        // User.findOne wont fire unlesss data is sent back
        process.nextTick(function() {
           User.findOne({'local.username' : username}, function(err, user) {
                // if there are any errors, return the error    
                if (err)
                    return done(err);

                if (user) {
                    return done(null, false, req.flash('signupMessage', 'That email is already taken.'))
                } else {
                    // if there is no user with that email
                    // create that user
                    var newUser = new User();

                    // set the user's local credentials

                    newUser.local.username = username;
                    newUser.local.password = newUser.generateHash(password);

                    // save the user
                    newUser.save(function(err) {
                        if (err)    
                            throw err;
                        return done(null, newUser);
                    })
                }
            }) 
        });
    }));

    // local login
    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, username, password, done) {
        User.findOne({'local.username':username}, function(err, user) {
            // if there are any errors
            if (err)    
                return done(err);
            // if no user is found
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found'));

            // if the user is found but wrong password
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Wrong Password!'));

            // all is well, return successful user
            return done(null, user);
        });
    }));
};

