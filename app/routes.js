// livelight-server
var livelight = require('./backends/livelight-server.js');

// routes
module.exports = function(app, passport, io) {

    //
    // Main Page
    //
    // index page
    app.get('/', function(req, res) {
        if(req.isAuthenticated())
        {
            livelight.getSessionStatus(req, function(response){
                res.render('index.ejs', {
                    user : req.user,
                    navActive: "index",
                    sessionTime: response.sessionTime,
                    videoLength: response.videoLength,
                    nextSessionHour: response.nextSessionHour
                });
            });
        }
        else
        {
            res.redirect('/login');
        }
    });

    //
    // Login
    //
    // show the login form
    app.get('/login', function(req, res) {
        livelight.getSessionStatus(req, function(response){
            res.render('login.ejs', {
                user : req.user,
                navActive: "login",
                sessionTime: response.sessionTime,
                videoLength: response.videoLength,
                nextSessionHour: response.nextSessionHour
            });
        });
    });

    // show sign up form
    app.get('/signup', function(req, res) {
       livelight.getSessionStatus(req, function(response){
            res.render('signup.ejs', {
                user : req.user,
                navActive: "signup",
                sessionTime: response.sessionTime,
                videoLength: response.videoLength,
                nextSessionHour: response.nextSessionHour
            });
        });
    });

    // show profile
    app.get('/profile', isLoggedIn, function(req, res){
       res.render('profile.ejs', {
           user : req.user 
        });
    });

    // logout
    app.get('/logout', function(req, res) {
       req.logout();
       res.redirect('/');
    });

    //
    // Result
    //
    app.get('/result', function(req, res) {
        res.render('result.ejs');
    });
    //
    // APIs
    //
    // api for signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash : true

    }));

    // api for login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/',
        failureRedirect : '/login',
        failureFlash : true

    }));
    
    // api for getting markers
    app.get('/getMarker', function(req, res) {
        livelight.getMarker(req, function(returnValue){
                res.send(returnValue);
            });
    });

    // api for setting markers
    app.post('/setMarker', function(req, res) {
        if (req.isUnauthenticated()) {
            res.send('');
        } else {
            livelight.setMarker(req, function(returnValue){
                
                if (returnValue.length > 1) { // Where cluster is made
                    io.sockets.emit('newCluster', returnValue);
                } else {                      // No cluster is made
                    res.send(returnValue);
                }
            });
        }
   });

    // api for dbscanning markers
    app.get('/clusterMarker', function(req, res) {
        livelight.clusterMarker(req, function(returnValue) {
            console.log('newCluster is made!');
            res.send(returnValue);
        });
    });

    // api for get all clusters
    app.get('/getCluster', function(req, res) {
        livelight.getCluster(req, function(returnCluster) {
            res.send(returnCluster);
        });
    });

    // api for undo markers
    //
    app.post('/undoMarker', function(req, res) {
        //livelight.undoMarker();
    });
    // Warning !!!! 
    // api for drop markers
    app.delete('/dropMarker', function(req, res) {
        livelight.removeMarker(req, function(response){
            res.send(response);
        });
    })
    // Warning !!!! 
    // api for drop clusters
    app.delete('/dropCluster', function(req, res) {
        livelight.removeCluster(req, function(response) {
            res.send(response);
        });
    });
    
    // api for archiving thumbnail
    app.post('/toThumbnail', function(req, res) {
        var base64Data = req.body.img;
        var clusterId = req.body.clusterId;
        livelight.saveThumbnail(req, clusterId, base64Data, function(returnValue) {
            res.send("Saved properly" + returnValue);
        });
    });

    // api for testClustering
    //
    app.post('/testCluster', function(req, res) {
        livelight.testCluster(req, function(returnValue, noise) {
            returnValue = [returnValue, noise]
            res.send(returnValue);
        });
    });

    // api for change tid
    //
    app.get('/changetid', function(req, res) {
        livelight.changetid(req, function() {
            console.log("changed properly");
            res.send("");
        });
    });

    app.get("/countpeople", function(req, res) {
        livelight.countpeople(req, function(doc) {
            res.send(doc);
        });
    });
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) 
        return next();
    res.redirect('/');
}
