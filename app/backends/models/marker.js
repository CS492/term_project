// app/models/marker.js
// marker schema
//

var mongoose = require('mongoose');

var markerSchema = mongoose.Schema({
    createdAt : Number,
    createdBy : String,
    tid : Number
})

module.exports = mongoose.model('Marker', markerSchema);
