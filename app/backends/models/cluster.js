// app/models/cluster.js
// cluster of highlight schema
//

var mongoose = require('mongoose');

var clusterSchema = mongoose.Schema({
    beginAt : Number,
    endAt   : Number,
    thumbnail : String,
    highlightID : Number
});

module.exports = mongoose.model('Cluster', clusterSchema);
