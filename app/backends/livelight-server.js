// Models
var Marker = require('./models/marker.js');
var Cluster = require('./models/cluster.js');

var clustering = require('density-clustering');
var dbscan = new clustering.DBSCAN();

// for DB normalization
var normalizeDB = function(rawSet) {
    normalizedDB = [];
    rawSet.forEach(function(nums){
        normalizedDB.push([nums.createdAt]);
    });
    return normalizedDB;
}

// api for getting markers
exports.getMarker = function(req, callback) {
    var tid = +req.query.tid;
    var qObj = {};
    if (tid) {
        qObj = {
            "tid" : tid       
        }
    }
    Marker.find(qObj, function(err, docs) {
        if (err)
            throw err;
        callback(docs);
    });
};

// api for setting markers
exports.setMarker = function(req, callback) {
    var date = new Date();
    var hour = date.getHours();
    // Setting up a marker
    var newMarker = new Marker();
    newMarker.createdAt = req.body.createdAt;
    newMarker.createdBy = req.user.local.username;
    newMarker.tid = req.body.tid; 
    newMarker.save(function(err) {
        if (err)    
            throw err;
    });
    callback(newMarker);

    // Setting up a cluster 
    var markers = [];
    var newClusters;
    var preClusters;
    
    Marker.find({tid : hour}, function(err, docs) {
        markers = docs;
        var normalizedDocs = normalizeDB(docs);
        var sampleRad = 10;
        var sampleNei = 3;
        newClusters = dbscan.run(normalizedDocs, sampleRad, sampleNei);
    }).then(function() {
        Cluster.find(
            {"highlightID" : {$gt : hour * 100, $lt : (hour + 1) * 100}}    
        ,
        function(err, docs) {
            preClusters = docs;
        }).then(function() {
            if (newClusters.length - preClusters.length > 1){ 
                var date = new Date();         // date for making id
                var newDoc = newClusters[newClusters.length - 2];
                var newClusterId = date.getHours() * 100 + preClusters.length + 1; // new cluster's id
                var newCluster = new Cluster(); // cluster going to be saved
                var returnValue = [
                    markers[newDoc[0]].createdAt-5, 
                    markers[newDoc[newDoc.length - 1]].createdAt+5, 
                    newClusterId
                ];


                newCluster.beginAt = returnValue[0];
                newCluster.endAt = returnValue[1];
                newCluster.highlightID = newClusterId;
                
                newCluster.save(function(err) {
                    if (err) throw err;
                    console.log("new Cluster Saved");
                    console.log(returnValue);
                    callback(returnValue);
                });
            }
        })
    });
};

// api for removing all markers collection
exports.removeMarker = function(req, callback) {
    Marker.remove({}, function(err) {
        if (err)  {
            console.log(err);
        } else {
            callback();
        }
    });
}
// api for get all clusters
//
exports.getCluster = function(req, callback) {
    var tid = +req.query.t;
    var order = +req.query.order;
    if (!order) {
        order = 0;
    }
    var qObj = {};
    // if tid is defined find only valid session markers
    if (tid) {
        qObj = {
            "highlightID" : {$gte : (tid) * 100 + order, $lt : (tid + 1) * 100}
        };
    }
    Cluster.find(qObj, function(err, docs) {
        if (err) {
            console.log(err);
            throw err;
        }


        callback(docs);
    });
}
// api for running DBSCAN algorithms
// !! Currently Not Using!
exports.clusterMarker = function(req, callback) {
    // get markers and 
    // normalize to fit it dataset
    // run DBSCAN algorithm
    var sampleRadius = 10;
    var sampleNeighbors = 2;
    var dbscanedItem = []

    Marker.find({}, function(err, docs) {
        docs = normalizeDB(docs);
        var dbscanedIndex = dbscan.run(docs, sampleRadius, sampleNeighbors);
        dbscanedIndex.forEach(function(item) {
            dbscanedItem.push([docs[item[0]][0], docs[item[item.length - 1]][0]]) 
        });
        Cluster.find({}, function(err, clusterDocs) {
            if (dbscanedItem.length - clusterDocs.length > 1) {
                var newDoc = dbscanedItem[dbscanedItem.length - 2];
                var newCluster = new Cluster();
                newCluster.beginAt = newDoc[0];
                newCluster.endAt = newDoc[1];
                
                newCluster.save(function(err) {
                    if (err)
                        throw err;
                    callback(newCluster);
                })
            }
        });
    })
}

// api for testing parameters
//
exports.testCluster = function(req, callback) {
    var reqRad = +req.body.radius;
    var reqNeighbors = +req.body.neighbors;
    var timeId = +req.body.tid;
    var qObj = {};
    if (timeId) {
        qObj.tid = timeId;
    }
    Marker.find(qObj, function(err, docs) {
        var dbscanedItem = [];
        docs.sort();
        docs = normalizeDB(docs);
        var dbscanedIndex = dbscan.run(docs, reqRad, reqNeighbors);
        dbscanedIndex.forEach(function(item) {
            dbscanedItem.push([docs[item[0]][0], docs[item[item.length - 1]][0]]) 
        });
        callback(dbscanedItem, dbscan.noise);
    });

}

// api for remove clusters 
exports.removeCluster = function(req, callback) {
    Cluster.remove({}, function(err){
        if (err) 
            throw err;
        callback();
    })    
}

// api for getting session status
exports.getSessionStatus = function(req, callback) {
    var videoLength = 754;
    var data = new Date();
    var sessionTime = data.getMinutes() * 60 + data.getSeconds();
    if(sessionTime >= videoLength)
        sessionTime = -1;

    var response = {};
    response.videoLength = videoLength;
    response.sessionTime = sessionTime;
    response.nextSessionHour = data.getHours() + 1;
    
    callback(response);
}



// api for saving thumbnail
//
exports.saveThumbnail = function(req, clusterId, thumbnailImg, callback) {
    Cluster.update(
        {highlightID : clusterId}, 
        {$set : {thumbnail : thumbnailImg}}, 
        function(err, updatedCluster){
             if (err) {
                 console.log(err);
             }
             callback(updatedCluster);

    });
}

exports.changetid = function(req, callback) {
    Marker.update(
            {tid:16},
            {$set : {tid : 20}},
            {multi : true},
            function(err) {
                console.log(err);
                callback();
            });
}

exports.countpeople = function(req, callback) {
    var tid = +req.query.tid;
    var qObj = {};

    if (tid) qObj.tid = tid;

    Marker.find(qObj).distinct("createdBy", function(err, doc) {
        if (err) throw err;
        callback(doc);
    })
}
